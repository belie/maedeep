#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri May 27 09:08:46 2022

@author: benjamin
"""

import os
import maedeep
import maedeep.dnn as mdp
import maedeep.parametric
import numpy as np
import h5py
import argparse

parser = argparse.ArgumentParser(description="Training model")
parser.add_argument("input_space", metavar="input space", type=str,
                    help="input space")
parser.add_argument("output_space", metavar="output space", type=str,
                    help="output space")
parser.add_argument('-d', '--dir', dest='directory', metavar='directory',
                    type=str, help='training data directory')
parser.add_argument('-ls', dest='layer_size',
                    metavar='hidden layer size', type=int, nargs='+',
                    help='size of the hidden layers (0=shallow network, default)', 
                    default=[0])
parser.add_argument('-a', '--act', dest='activation_function',
                    metavar='activation function', type=str,
                    help='activation function (default=sigmoid)', 
                    default=["sigmoid"])
parser.add_argument('-ne', dest='num_epochs', type=int,
                    metavar='number of epochs',
                    help='number of epochs (default=15)', default=15)
parser.add_argument('-bs', '--batch-size', dest='batch_size', type=int, 
                    metavar='batch size',
                    help='batch size (default=32)', 
                    default=32)
parser.add_argument('-o', '--output', dest='output_model', type=str, 
                    metavar='output model',
                    help='path to save the model')
parser.add_argument('-v', '--verbosity', dest='verbosity', type=int, 
                    metavar='verbosity',
                    help='verbosity level (default=0)', default=0)
parser.add_argument('-kv', '--keep-vowels', dest='vowels', type=int, 
                    metavar='keep vowels',
                    help='keeping just vowels (default=0)', default=0)
parser.add_argument('-vf', '--vowel-file', dest='vowel_file', type=str, 
                    metavar='vowel file',
                    help='path to the vowel file (default=data_vowels.h5',
                    default="data_vowels.h5")
parser.add_argument('-ko', '--keep-opening', dest='opening', type=float, 
                    metavar='keep opens',
                    help='keeping just open VT (default=-1, keep all)', 
                    default=-1)
parser.add_argument('-p', '--test', dest='proportional_test',
                    type=float, metavar='percentage',
                    help='percentage of data for test (default=5%)', 
                    default=5)
parser.add_argument('-t', '--train', dest='proportional_training',
                    type=float, metavar='percentage_training',
                    help='percentage of data for training (default=100%)', 
                    default=100)
parser.add_argument('-pv', '--valid', dest='proportional_validation',
                    type=float, metavar='percentage_validation',
                    help='percentage of data for validation (default=5%)', 
                    default=5)

input_space = parser.parse_args().input_space
output_space = parser.parse_args().output_space
model_file = parser.parse_args().output_model
data_directory = parser.parse_args().directory
hidden_layer_size = parser.parse_args().layer_size
activation_function = parser.parse_args().activation_function
nb_epochs = parser.parse_args().num_epochs
batch_size = parser.parse_args().batch_size
verbosity = parser.parse_args().verbosity
isJustVowels = parser.parse_args().vowels
opening = parser.parse_args().opening
vowel_file = parser.parse_args().vowel_file
prop_test = parser.parse_args().proportional_test
prop_training = parser.parse_args().proportional_training
prop_validation = parser.parse_args().proportional_validation

if verbosity:
    print("Training DNN model of " + input_space + " to " + output_space + 
          " mapping")
    print("Parameters: ")
    print("Directory of training data: ", data_directory)
    if hidden_layer_size[0] > 0:
        print("Size of hidden layers: ", hidden_layer_size)
    else:
        print("No hidden layer")
    print("Activation function: ", activation_function)
    print("Number of epochs: ", nb_epochs)
    print("Batch size: ", batch_size)
    print("Proportion of training data: ", prop_training, "%")
    print("Proportion of test data: ", prop_test, "%")
    print("Proportion of validation data: ", prop_validation, "%")
    if isJustVowels:
        print("Training only on vowel VT configurations")
        print("Vowels are taken from: ", vowel_file)
    if opening > 0:
        print("Training only on open VT configurations")
        print("Minimal opening :", opening, " cm")

def cost_function(generated, observed, relative=False):    
    if relative:
        return np.mean(np.abs((generated-observed)/observed))
    else:
        return np.mean(np.abs((generated-observed)))
    
def prepare_training_data(data_directory, keys, prop_training=100):
    
    list_files = os.listdir(data_directory)
    datas = [[]]*len(keys)
    if prop_training < 100:
        n = int(len(list_files)*prop_training/100)
        list_files = list_files[:n]
    for l in list_files:
        file = os.path.join(data_directory, l)
        with h5py.File(file, "r") as hf:            
            data = [hf[key][()].T.astype("f") for key in keys]
            datas = [x + list(y) for x, y in zip(datas, data)] 
            
    return [np.array(d).astype("f") for d in datas]

def extract_vowel_indices(formants, vowel_file):
    
    with h5py.File(vowel_file, "r") as hf:
        frequency = hf["frequency"][()].T
    return maedeep.in_hull_delaunay(formants, frequency)

def extract_vowel(x, idx):
    return x[:, idx]

def extract_open_vt(tasks, opening=0):
    task_aperture = np.array([tasks[:, i] for i in [0, 2, 4]]).T
    return [i for i in range(tasks.shape[0]) if (task_aperture[i, :]>opening).all()]
    
def choose_keys(space):
    if space == "articulatory":
        return ["articulators"]
    elif space == "contour":
        return ["lower_contour_x", "lower_contour_y", 
                "upper_contour_x", "upper_contour_y"]
    elif space == "area":
        return ["area_function", "length_function"]
    elif space == "task":
        return ["tract"]
    elif space == "acoustic":
        return ["formants"]
    
def extract_data(space, datas):
    if space in ["articulatory", "task", "acoustic"]:
        x = [datas[0]]
    elif space == "contour":
        x = datas[:4]
    elif space == "area":
        x = datas[:2]
    return x, datas[len(x):]

def extract_training_data(space, data):
    if space in ["articulatory", "task", "acoustic"]:
        x = data[0]
    elif space == "contour":
        low_x, low_y, up_x, up_y = data
        x = np.array(list(low_x.T) + list(low_y.T) + list(up_x.T) + list(up_y.T)).T.astype("f")
        del up_y, up_x, low_x, low_y
    elif space == "area":
        af, lf = data
        x = np.array(list(af.T) + list(lf.T)).T.astype("f")
    return x
        
keys, output_key = [choose_keys(key) for key in [input_space, output_space]]

for key in output_key:
    keys += [key]

if verbosity:    
    print("Preparing training data...")

datas = prepare_training_data(data_directory, keys, 
                              prop_training=prop_training)
input_data, datas = extract_data(input_space, datas)
output_data = extract_data(output_space, datas)[0]
if verbosity:
    print("Data extracted (" + str(input_data[0].shape[0]) + " samples)")
    
del datas

if isJustVowels > 0:
    isJustVowels = True
else:
    isJustVowels = False
    
if isJustVowels:    
    if verbosity:
        print("Extracting vowels...")
    formants = prepare_training_data(data_directory, ["formants"], 
                                     prop_training=prop_training)[0]
    indices = extract_vowel_indices(formants, vowel_file)
    datas = [d.T for d in input_data]
    for d in output_data:
        datas.append(d.T)
    datas = [extract_vowel(d, indices).T for d in datas] 
    input_data = datas[:len(input_data)]
    output_data = datas[len(input_data):]
    if verbosity:
        print("Vowels extracted (" + str(input_data[0].shape[0]) + " samples)")
    del datas, indices

if opening >= 0 and not isJustVowels:
    if verbosity:
        print("Extracting open configurations...")
    tasks = prepare_training_data(data_directory, ["tract"], 
                                  prop_training=prop_training)[0]    
    indices = extract_open_vt(tasks, opening=opening)
    datas = [d.T for d in input_data]
    for d in output_data:
        datas.append(d.T)
    datas = [extract_vowel(d, indices).T for d in datas] 
    input_data = datas[:len(input_data)]
    output_data = datas[len(input_data):]
    if verbosity:
        print("Open configurations extracted (" + str(input_data[0].shape[0]) + " samples)")
    del datas, indices

x, y = [extract_training_data(space, data) 
        for space, data in zip([input_space, output_space],
                               [input_data, output_data])]

if input_space == "articulatory":
    minx = 0
    maxx = 3
elif input_space in ["task", "acoustic", "area"]:
    minx = np.min(x, axis=0, keepdims=True)
    maxx = np.max(x - minx, axis=0, keepdims=True)
x = (x - minx) / maxx
    
if output_space == "articulatory":
    miny = 0
    maxy = 3
elif output_space in ["task", "acoustic", "area"]:
    miny = np.min(y, axis=0, keepdims=True)
    maxy = np.max(y - miny, axis=0, keepdims=True)
y = (y - miny) / maxy

del input_data, output_data

(x_train, y_train, 
 x_test, y_test, 
 x_valid, y_valid) = mdp.make_test(x.T, y.T,
                                   prop_test=prop_test,
                                   prop_valid=prop_validation)

x_valid = x_valid * maxx + minx
y_valid = y_valid * maxy + miny 
                                   
nb_gen = x_valid.shape[0]
if verbosity:                                   
    print("Training and test datasets created")
    print("Number of training samples: ", x_train.shape[0])
    print("Number of test samples: ", x_test.shape[0])
    print("Number of validation samples: ", x_valid.shape[0])

nb_sampl, nb_feat_input = x_train.shape
nb_feat_output = y_train.shape[1]
loss_function = "mean_squared_error"

if not hidden_layer_size[0] > 0:
    encoder = mdp.build_model(nb_feat_input, nb_feat_output, 
                                  encoding_function=activation_function,
                                  decoding_function=activation_function,
                                  loss=loss_function)
else:
    encoder = mdp.build_dnn_model(nb_feat_input, nb_feat_output, 
                                  hidden_layer_size=hidden_layer_size,
                                  activation_function=activation_function,
                                  loss_function=loss_function)

encoder.fit(x_train, y_train,
                epochs=nb_epochs,
                batch_size=batch_size,
                shuffle=True,
                validation_data=(x_test, y_test),
                verbose=verbosity)

print("Model has been successfully trained")
print("Validating on " + str(nb_gen) + " samples...")

y_predict = encoder.predict((x_valid - minx)/maxx, 
                            verbose=verbosity) * maxy + miny
if output_space == "acoustic":
    relative = True
else:
    relative = False
    
error = cost_function(y_predict, y_valid, relative=relative)

if verbosity > 0:
    print("Error: ", error)
    
mdp.save_model(encoder, model_file)

with h5py.File(model_file, "a") as hf:
    hf.create_dataset("input", data=input_space)
    hf.create_dataset("output", data=output_space)
    hf.create_dataset("normalization_factors_input", data=(maxx, minx))
    hf.create_dataset("normalization_factors_output", data=(maxy, miny))
    hf.create_dataset("activation_function", data=activation_function)
    hf.create_dataset("loss_function", data=loss_function)
    hf.create_dataset("number_of_epochs", data=nb_epochs)
    hf.create_dataset("errors", data=error)
    hf.create_dataset("convergence", data=True)

if verbosity > 0:
    print("Model successfully saved in ", model_file)
