#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul 18 11:09:16 2022

@author: benjamin
"""

import argparse
import h5py
import os
from tqdm import tqdm
import numpy as np

parser = argparse.ArgumentParser(description=""" Prepare training data """)
parser.add_argument("data_directory", metavar="data_directory", type=str,
                    help="Directory containing data files")
parser.add_argument("-o", dest="output", metavar="output", type=str,
                    help="output directory")
parser.add_argument("-v", dest="verbosity", metavar="verbosity", type=int,
                    default=1,
                    help="verbosity (default=1, display progress bar)")
parser.add_argument('-bs', '--batch-size', dest='batch_size', type=int, 
                    metavar='batch size',
                    help='batch size (default=1e6)', 
                    default=1000000)

data_directory = parser.parse_args().data_directory
output_dir = parser.parse_args().output
verbosity = parser.parse_args().verbosity
batch_size = parser.parse_args().batch_size

if os.path.isdir(output_dir):
    print(output_dir + " exists")
else:
    raise ValueError(output_dir + " does not exist")
    
output_file = os.path.join(output_dir, "training_data")

list_files = os.listdir(data_directory)

def remove_rejected(x, rejected, axis=1):
    return np.delete(x, rejected, axis=axis)
   
def check_size(data, batch_size):
    curr_size = np.array(data[0]).shape[0]
    return curr_size >= batch_size
    
keys = ["articulators", "lower_contour_x", "lower_contour_y", 
        "upper_contour_x", "upper_contour_y", "area_function", 
        "length_function", "tract", "formants"]

datas = [[]]*len(keys)
if verbosity > 0:
    disable = False
else:
    disable = True

nb_file = len(list_files)
n = 0
curr_size = 0

for l in tqdm(list_files):
    full_file = os.path.join(data_directory, l)   
    
    try:
        with h5py.File(full_file, "r") as hf:
            data = [hf[key][()].T for key in keys]       
            formants = data[-1].T
            idx = [i for i in range(formants.shape[1]) if np.isnan(formants[:, i]).any()]
            rejected = np.unique(idx)
            
        if len(rejected) > 0:
            data = [remove_rejected(x, rejected, axis=0) for x in data]
        datas = [x + list(y) for x, y in zip(datas, data)]
        del data, rejected, idx, formants   
    except:
        pass
    
    if check_size(datas, batch_size):
        while os.path.isfile(output_file + "_%.4d" %(n+1) + ".h5"):
            n += 1
        print(n)
        with h5py.File(output_file + "_%.4d" %(n+1) + ".h5", "w") as hf:
            [hf.create_dataset(key, data=np.array(value).T) for key, value in zip(keys, datas)]
            datas = [[]]*len(keys)
            n += 1

while os.path.isfile(output_file + "_%.4d" %(n+1) + ".h5"):
    n += 1            
with h5py.File(output_file + "_%.4d" %(n+1) + ".h5", "w") as hf:
    [hf.create_dataset(key, data=np.array(value).T) for key, value in zip(keys, datas)]