#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu May 26 11:39:24 2022

@author: benjamin
"""


import os
import sys
import warnings
if not sys.warnoptions:
    warnings.simplefilter("ignore")
    os.environ["PYTHONWARNINGS"] = "ignore"
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2' 

import maedeep.parametric
import numpy as np
from tqdm import tqdm
import h5py
import argparse

parser = argparse.ArgumentParser(description=""" Generate training data for
                                  articulatory mapping""")
parser.add_argument("-o", dest="output_file", metavar="output_file", 
                    type=str, help="file where to store training data (.h5)")
parser.add_argument("-n", dest="nb_samples", metavar="nb_samples", type=int,
                    default=100, 
                    help=""" Number of samples generated (default=100) """)

def rejection(x, n):
    if len(n) > 0:
        return np.delete(x, n, axis=1)
    else:
        return x

import time
start = time.time()
output_file = parser.parse_args().output_file
n_samples = parser.parse_args().nb_samples

print("Data will be stored in " + output_file)
print("Number of VT configurations: ", n_samples)

c = 0.55

art_params = 6*np.random.rand(7, n_samples) - 3
   
rejected = []
origin = (10, 10)
x_down = np.zeros((29, n_samples))
y_down = np.zeros((29, n_samples))
x_up = np.zeros((29, n_samples))
y_up = np.zeros((29, n_samples))
    
contours = maedeep.parametric.articulatory_to_contour(art_params)
af = maedeep.parametric.contour_to_area(contours)
tasks = maedeep.parametric.contour_to_task(contours, c)
formants = maedeep.parametric.area_to_formant(af)
area_function = af.area
length_function = af.length
for n in range(n_samples):    
    xd, yd, xu, yu = contours[n].contours
    x_down[:, n] = xd
    y_down[:, n] = yd
    x_up[:, n] = xu
    y_up[:, n] = yu

rejected = [i for i in range(n_samples) if np.isnan(formants[:, i]).any()]

print("Data successfully generated")
print("Rejection: ", len(rejected)) 

with h5py.File(output_file, "w") as hf:
    hf.create_dataset("articulators", data=rejection(art_params, rejected))
    hf.create_dataset("tract", data=rejection(tasks, rejected))
    hf.create_dataset("formants", data=rejection(formants, rejected))
    hf.create_dataset("lower_contour_x", data=rejection(x_down, rejected))
    hf.create_dataset("lower_contour_y", data=rejection(y_down, rejected))
    hf.create_dataset("upper_contour_x", data=rejection(x_up, rejected))
    hf.create_dataset("upper_contour_y", data=rejection(y_up, rejected))
    hf.create_dataset("area_function", data=rejection(area_function, rejected))
    hf.create_dataset("length_function", data=rejection(length_function, rejected))  
    hf.create_dataset("rejection", data=rejected)  

print("Data successfully saved in ", output_file)

print("Elapsed time: ", time.time() - start)
