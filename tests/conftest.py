import os
import tempfile
import pytest
from maedeep import (_config)
from maedeep.parametric import (modeltools, _forwardmapping)

@pytest.fixture
def home():
    """Sets and returns $HOME to a temporary directory."""
    with tempfile.TemporaryDirectory() as temphome:
        old_home = os.environ.get('HOME')
        os.environ['HOME'] = temphome
        try:
            yield temphome
        finally:
            if old_home:
                os.environ['HOME'] = old_home

@pytest.fixture
def datapath():
    """Returns the path to data directory alongside tests."""
    return os.path.join(_config.get_maedeep_path().parent, 'tests','data')

@pytest.fixture
def model():
    """Returns the path to test data file."""
    return os.path.join(_config.get_maedeep_path().parent, 'tests', 
                        'data', 'model_spec.json')

@pytest.fixture
def datamodel(model):
    """Returns the test data """    
    return modeltools.check_model(model)

@pytest.fixture
def articulatory_parameters():
    """Returns the articulatory parameters for tests"""    
    return [0]*7

@pytest.fixture
def contours(articulatory_parameters, model):
    """Returns the contours for tests"""    
    return _forwardmapping.articulatory_to_contour(articulatory_parameters, model)

@pytest.fixture
def area_function(articulatory_parameters, model):
    """Returns the areas for tests"""    
    return _forwardmapping.articulatory_to_area(articulatory_parameters, model)

@pytest.fixture
def transfer_function(articulatory_parameters, model):
    """Returns the transfer functions for tests"""    
    return _forwardmapping.articulatory_to_transfer_function(articulatory_parameters, model)

@pytest.fixture
def tasks(articulatory_parameters, model):
    """Returns the tasks for tests"""    
    return _forwardmapping.articulatory_to_task(articulatory_parameters, model)

@pytest.fixture
def formants(articulatory_parameters, model):
    """Returns the formants for tests"""    
    return _forwardmapping.articulatory_to_formant(articulatory_parameters, model)

@pytest.fixture
def coeff(datamodel):
    """Returns the size coefficient """   
    size_correction = datamodel["semi-polar coordinates"]["size correction"]    
    vp_map = datamodel["semi-polar coordinates"]["map coeff"]
    return size_correction * vp_map
