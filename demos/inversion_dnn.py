#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jul 21 16:35:25 2022

@author: benjamin
"""

import maedeep as mdp
from maedeep import parametric
from maedeep import dnn
import spychhiker as sph
import os
import numpy as np

""" This demo estimates the articulatory parameters from a real speech signal.
The speech isgnal is the psuedoword azha """

### Read the the audio file and estimate formants using SpeechHiker
test_signal = os.path.join("data", "azha.wav")
sp = sph.file2speech(test_signal, sro=10000)
formants = sp.formantestimate()
tfo = formants.time_vector
dt = np.median(np.diff(tfo))*1000
tfo = np.arange(0, tfo[-1]+1/200, 1/200)
dt = 1/200

""" Vocal Tract Length Normalization to fit the VT length 
of the articulatory model """
formants.interpolate(tfo)
frequency = formants.frequency

length = mdp.vtl_estimation(frequency.T)
# frequency = mdp.vtln(frequency)

sp.computespectrogram(64)

""" Performs acoustic-to-articulatory mapping and computes shape, tasks and formants """
articulatory_params = mdp.dnn.acoustic_to_articulatory(frequency.T)
contours = mdp.parametric.articulatory_to_contour(articulatory_params)
tasks = mdp.dnn.articulatory_to_task(articulatory_params)
acoustics = mdp.dnn.articulatory_to_formant(articulatory_params).T

import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation

def draw_contour(y, idx, line="b"):
    low_x, low_y, up_x, up_y = y[idx].contours
    low_y = correct_closure(low_y, up_y, low_x)
    plt.plot(low_x, low_y, line)
    plt.plot(up_x, up_y, line)
    plt.xlim([0,20])
    plt.ylim([0,20])
    
def correct_closure(low_y, up_y, low_x):
    idx = [i for i in range(len(low_y)) if low_y[i] > up_y[i] and low_x[i] < 10]
    low_y[idx] = up_y[idx]
    return low_y

nb_smpl = len(tfo)
plt.close("all")
""" Plot spectrogram of real speech with formant trajectories """
sp.spectrogram.tfplot(formant_plot=True)

""" Plot formant trajectories of real speech and formant of estimated VTs """
plt.figure()
plt.plot(tfo, frequency.T, 'b', label="Real speech")
plt.plot(tfo, acoustics, '--r', label="Estimated")
plt.xlabel("Time (s)")
plt.ylabel("Frequency (Hz)")

""" Plot tasks """
keys = ["Lip aperture (cm)", "Lip protrusion (cm)",
        "Tongue tip constriction degree (cm)",
        "Tongue tip constriction location (degree)",
        "Tongue dorsum constriction degree (cm)",
        "Tongue dorsum constriction location (degree)",
        "Length (m)"]

plt.figure()
for n in range(7):
    plt.subplot(3, 3, n+1)
    plt.plot(tasks[n, :])
    plt.xlabel("Time (s)")
    plt.ylabel(keys[n])
    
""" Plot articulatory parameters """
keys = ["Jaw position", "Position of tongue dorsum", "Height of tongue dorsum",
        "Position of the tongue tip", "Lip aperture", "Lip protrusion", 
        "larynx height"]
plt.figure()
for n in range(7):
    plt.subplot(3, 3, n+1)
    plt.plot(articulatory_params[n, :])
    plt.xlabel("Time (s)")
    plt.ylabel(keys[n])

""" Plot animation """
fig, ax = plt.subplots()
def animate(idx):
    ax.clear()
    draw_contour(contours, idx)
ani = FuncAnimation(fig, animate, frames=nb_smpl, interval=dt, repeat=True)


plt.show()
