#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Aug  2 09:28:39 2022

@author: benjamin
"""

import maedeep.parametric
import matplotlib.pyplot as plt
import numpy as np
plt.close("all")


# Function to draw a contour
def draw_contour(contour, line="b"):
    low_x, low_y, up_x, up_y = contour.contours
    plt.plot(low_x, low_y, line)
    plt.plot(up_x, up_y, line)
    plt.xlim([0,20])
    plt.ylim([0,20])
    
# Function to plot AF
def plot_af(area_function, line="b"):
    area, length = (AF.area, AF.length)
    plt.plot(100*np.cumsum(length, axis=0), area*1e4, line)
    plt.xlabel("Distance from glottis (cm)")
    plt.ylabel("Area (cm²)")
    
def db(x):
    return 20*np.log10(np.abs(x))    
    
""" This demo plots contours, area function, transfer function and formants
 of the neutral VT configuration, using the parametric model """

p = [0]*7 # Neutral configuration

""" Compute and draw contours """
contours = maedeep.parametric.articulatory_to_contour(p)
plt.figure()
plt.subplot(221)
draw_contour(contours[0])

""" Compute and draw area function """
AF = maedeep.parametric.articulatory_to_area(p)
plt.subplot(222)
plot_af(AF)

""" Compute and plot transfer function at frequency interval of 1 Hz """
tf, freq = maedeep.parametric.articulatory_to_transfer_function(p, df=1)
# tf, freq = interpolate_tf(tf, freq, 5001) 
plt.subplot(223)
plt.plot(freq, db(tf), "b")
plt.xlabel("Frequency (Hz)")
plt.ylabel("|TF| (dB)")

""" Compute formants and plot formants on TF """
formants = maedeep.parametric.articulatory_to_formant(p)
plt.subplot(223)
print("Formants (Hz):")
keys = ["F1: ", "F2: ", "F3: ", "F4: "]
i = 0
for key, value in zip(keys, formants):
    print(key, value[0])
    idx = np.argmin(np.abs(freq - value[0]))
    plt.plot(freq[idx], db(tf[idx]), 'or')

plt.show()

""" Compute tasks """
tasks = maedeep.parametric.articulatory_to_task(p)
print("Tasks:")
keys = ["Lip aperture (cm): ", "Lip protrusion (cm): ",
        "Tongue tip constriction degree (cm): ",
        "Tongue tip constriction location (degree): ",
        "Tongue dorsum constriction degree (cm): ",
        "Tongue dorsum constriction location (degree): ",
        "Length (m): "]
for key, value in zip(keys, tasks):
    print(key, value[0])